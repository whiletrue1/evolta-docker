PASSWORD=`cat /run/secrets/postgres_activity_password`
psql -t <<SQL
CREATE USER activity WITH PASSWORD '$PASSWORD';
CREATE DATABASE activity OWNER activity;
SQL
