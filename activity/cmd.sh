#!/bin/sh
if [ $APP_ENV == dev ]; then
  composer install
else
  composer install --no-dev --optimize-autoloader
fi

chown -R nginx:nginx /var/www/var

APP_SECRET=`cat /run/secrets/activity_app_secret` \
DB_PASS=`cat /run/secrets/postgres_activity_password` \
php-fpm7

nginx
tail -F /var/log/nginx/*.log /var/log/php7/error.log
#while true; do date; sleep 5; done
